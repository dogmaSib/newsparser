import configparser
import os

def create_default_config(path):
    """
     Create default config file
    """
    settings = []

    config_map = {"sitename":"lenta","title_tag":"h1","title_attr_first":"itemprop","title_attr_second":"headline", "content_tag":"div", "content_attr":"articleBody"}

    settings.append(config_map)

    config_map = {"sitename":"ria","title_tag":"h1","title_attr_first":"itemprop", "title_attr_second":"name", "content_tag":"div", "content_attr":"articleBody"}

    settings.append(config_map)

    config = configparser.ConfigParser()

    for setting in settings:
        config.add_section(setting.get("sitename") )
        config.set(setting.get("sitename"), "title_tag", setting.get("title_tag"))
        config.set(setting.get("sitename"), "title_attr_first", setting.get("title_attr_first"))
        config.set(setting.get("sitename"), "title_attr_second", setting.get("title_attr_second"))
        config.set(setting.get("sitename"), "content_tag", setting.get("content_tag"))
        config.set(setting.get("sitename"), "content_attr", setting.get("content_attr"))

    with open(path, "w") as config_file:
        config.write(config_file)

def get_config(path):
    """
    Returns the config object
    """
    if not os.path.exists(path):
        create_default_config(path)

    config = configparser.ConfigParser()
    config.read(path)
    return config


def get_setting(path, section, setting):
    """
    Print out a setting
    """
    config = get_config(path)
    value = config.get(section, setting)
    msg = "{section} {setting} is {value}".format(
        section=section, setting=setting, value=value
    )

    print(msg)
    return value

#TODO: this functions for future
def update_setting(path, section, setting, value):
    """
    Update a setting
    """
    config = get_config(path)
    config.set(section, setting, value)
    with open(path, "w") as config_file:
        config.write(config_file)


def delete_setting(path, section, setting):
    """
    Delete a setting
    """
    config = get_config(path)
    config.remove_option(section, setting)
    with open(path, "w") as config_file:
        config.write(config_file)


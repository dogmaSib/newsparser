from cx_Freeze import setup, Executable

executables = [Executable('__init__.py', targetName='news_parser.exe')]

excludes = ['logging', 'unittest', 'xml', 'bz2']

zip_include_packages = []

include_files = ['data']

options = {
    'build_exe': {
        'include_msvcr': True,
        'excludes': excludes,
        'zip_include_packages': zip_include_packages,
        'build_exe': 'build_windows',
        'include_files': include_files,
    }
}

setup(name='news_parser',
      version='0.0.10',
      description='Parser for news',
      executables=executables,
      options=options)
import readurl
import attrsettings


def resolve_url(site_name):
    row = site_name.split("/")[2]
    return str(row).replace("www.", "").split(".")[0]


if __name__ == "__main__":
    path = "data/settings.ini"

    print("enter the web page name starting with 'http://': ")
    url = input()
    # get all settings
    attrsettings.get_config("data/settings.ini")

    #get settings from file by site name
    setting_path = resolve_url(url)
    site_config = {}
    # creating map with args for parsing
    config_paths = ["title_tag", "title_attr_first", "title_attr_second", "content_tag", "content_attr"]
    for config_name in config_paths:
        site_config.setdefault(config_name, attrsettings.get_setting(path, setting_path, config_name))
    print(site_config)

    reader = readurl.SiteNewsParser(str(url).strip(), site_config)




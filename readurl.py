from html.parser import HTMLParser
from urllib.request import urlopen
from os.path import exists
from os import makedirs


class SiteNewsParser(HTMLParser):

    def error(self, message):
        pass

    def __init__(self, url, config, *args, **kwargs):
        self.content_type = 0
        #attributes map for parsing site
        self.attr_names = config

        self.dublicate_tag = 0

        self.is_script = False
        self.is_paragraph = False
        # map parsing results
        self.itemNews = []
        # link to news
        self.url = url
        # call parent __init__
        super().__init__(*args, **kwargs)
        # get all content from site
        self.feed(self.read_site_content())
        # write all data in to file
        self.write_to_file()

    def handle_starttag(self, tag, attrs):
        # check for contains tag in our config map
        if tag == self.attr_names.get("title_tag") or tag == self.attr_names.get("content_tag"):
            # checking attributes
            if self.content_type == 2 :
                self.dublicate_tag = self.dublicate_tag + 1
            else:
                for attr in attrs:
                    if attr[0] == self.attr_names.get("title_attr_first") and attr[1] == self.attr_names.get("title_attr_second"):
                        self.content_type = 1
                    if attr[1] == self.attr_names.get("content_attr"):
                        self.content_type = 2
        # do not append data if its css style or script
        if tag == "script" or tag == "style":
            self.is_script = True
        if tag == "a" and self.content_type != 0:
            for attr in attrs:
                if attr[0] == "href":
                    self.itemNews.append("[ "+attr[1]+" ]")
        if tag == "p" and self.content_type != 0:
            self.is_paragraph = True



    # checking end tag (e.g. </div>) for body, because content may contains a lot of <div> tags
    def handle_endtag(self, tag):
        if tag == self.attr_names.get("content_tag"):
            if self.dublicate_tag == 0 and self.content_type == 2:
                self.content_type = 0
                self.dublicate_tag = 0
            else:
                if self.content_type == 2:
                    self.dublicate_tag = self.dublicate_tag - 1

    def handle_data(self, data):
        if self.is_script:
            self.is_script = False
            return
        if self.content_type == 1:
            self.itemNews.append(data.strip())
            self.itemNews.append('\n\n')
            self.content_type = 0
        if self.content_type == 2:
            data = data.strip().replace('\r', '').replace('\n', '')
            i = 0
            appended = ''
            while i < len(data)/80:
                appended += data[i*80:(i+1)*80] + '\n'
                i += 1
            appended += data[i*79:]
            self.itemNews.append(appended)
            if self.is_paragraph:
                self.is_paragraph = False
                self.itemNews.append('\n')

    def read_site_content(self):
        resp = urlopen(self.url)
        respData = resp.read()
        respDataDecode = ''
        try:
            respDataDecode = respData.decode('utf-8')
        except UnicodeDecodeError:
            respDataDecode = respData.decode('windows-1251')
        return respDataDecode

    def write_to_file(self):
        # creating directories
        directory = ''
        sep = '/'
        dirs = self.url.replace("https://", "").replace("http://", "").split(sep)
        i = 0
        while i < len(dirs) - 1:
            if dirs[i] != "":
                directory = directory + dirs[i] + "/"
            i += 1

        filename = dirs[len(dirs)-1]
        if filename == "":
            filename = dirs[len(dirs)-2]

        filename = str(filename).replace(".html", "").replace(".php", "").replace(".htm", "").replace(".shtml", "").replace("?relap=1","")

        if not exists(directory):
            makedirs(directory)

        f = open(directory + filename + '.txt', 'w', encoding="utf-8")
        # write all elements separated \n
        f.write(''.join(self.itemNews))
        # close file
        f.close()